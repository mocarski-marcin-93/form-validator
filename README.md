### How do I get set up? ###

1. Set your form *id* to *form-validator* 
2. Set your inputs *classes* to *form-validator secondary* when *secondary* equals:

* name or
* email or
* password or
* phone number or
* postal code or
* integer or
* float.