$(document).ready(function(e) {
    var form = new FormValidator("form-validator");
});



// Jakie inputy?
// - name - tylko litery
// - email - granicznej postaci: a0.a0.a0@a.a
// - password - tylko litery i cyfry
// - date - 7 lub 8 cyfr, dozwolone kropki, 'r', myślniki, ktore zostaną wycięte
// - phone-number - od 9 do 12 vydr, dozwolone myślniki, spacje, nawiasy, plusy, ktore zostaną wycięte
// - postal-code - [|||--loading-----]
// - integer - wiadomo
// = float - wiadomo
function FormValidator(formId) {
    this.form = document.getElementById(formId);
    this.successTable = [];

    this.loadForm();
    this.listenForm();
}

FormValidator.prototype.loadForm = function() {
    if (!(this.allInputs = $(this.form).find('.input-validator')).length) {
        throw "Pusty form!";
    }
    this.names = $(this.form).find('.input-validator.name');
    this.emails = $(this.form).find('.input-validator.email');
    this.passwords = $(this.form).find('.input-validator.password');
    this.dates = $(this.form).find('.input-validator.date');
    this.phoneNumbers = $(this.form).find('.input-validator.phone-number');
    this.postalCodes = $(this.form).find('.input-validator.postal-code');
    this.integers = $(this.form).find('.input-validator.integer');
    this.floats = $(this.form).find('.input-validator.float');

    // Button czerwony dopoki nie wypełnimy pol
    var newStyles = document.createElement('STYLE');
    newStyles.innerHTML = '.input-validator-button-not-ready:hover {' +
        '                     background: #f77;' +
        '                     border-color: #f77;' +
        '                  }' +
        '                  .input-validator-button-ready:hover {' +
        '                     background: #7f7;' +
        '                     border-color: #7f7;' +
        '                  }';
    document.getElementsByTagName('HEAD')[0].appendChild(newStyles);

    // Keyframes dla animacji buttona gdy form gotowy do wysłania
    var newStyles = document.createElement('STYLE');
    newStyles.innerHTML = '@keyframes printReady {' +
        '                     0% {box-shadow: none;}' +
        '                     25% {box-shadow: 0 0 0 5px #7f7;}' +
        '                     100% {box-shadow: none;}' +
        '                  }';
    document.getElementsByTagName('HEAD')[0].appendChild(newStyles);
};

FormValidator.prototype.listenForm = function() {
    // Podczepiamy keyframes dla focusina
    // var keyframesFocus = document.createElement("STYLE");
    // keyframesFocus.innerHTML = "@keyframes printFocus {" +
    //     "                         0% {background: #fff;}" +
    //     "                         100% {background: #CCCCFF;}" +
    //     "                        }";
    // document.getElementsByTagName("HEAD")[0].appendChild(keyframesFocus);

    var _this = this;
    $(this.allInputs).on("focusin", function(e) {
        //  $(this).css('animation', 'printFocus 2s forwards');
        $(this).one("focusout", function(e) {
            //console.log(this);
            _this.validate(this);
            _this.checkIfReady();
        });
    });
};

FormValidator.prototype.validate = function(input) {
    var _this = this;
    if ($(input).hasClass('name')) {
        _this.checkName(input) ? _this.printSuccesss(input) : _this.printFailure(input);
    }
    if ($(input).hasClass('email')) {
        _this.checkEmail(input) ? _this.printSuccesss(input) : _this.printFailure(input);
    }
    if ($(input).hasClass('password')) {
        _this.checkPassword(input) ? _this.printSuccesss(input) : _this.printFailure(input);
    }
    if ($(input).hasClass('date')) {
        _this.checkDate(input) ? _this.printSuccesss(input) : _this.printFailure(input);
    }
    if ($(input).hasClass('phone-number')) {
        _this.checkPhoneNumber(input) ? _this.printSuccesss(input) : _this.printFailure(input);
    }
    if ($(input).hasClass('postal-code')) {
        _this.checkPostalCode(input) ? _this.printSuccesss(input) : _this.printFailure(input);
    }
    if ($(input).hasClass('integer')) {
        _this.checkInteger(input) ? _this.printSuccesss(input) : _this.printFailure(input);
    }
    if ($(input).hasClass('float')) {
        _this.checkFloat(input) ? _this.printSuccesss(input) : _this.printFailure(input);
    }
};

FormValidator.prototype.checkIfReady = function() {
    if (this.successTable.length >= this.allInputs.length) {
        $('.input-validator-button-not-ready').attr('class', 'input-validator-button-ready').css('animation', 'printReady 3s infinite');
    } else {
        $('.input-validator-button-ready').attr('class', 'input-validator-button-not-ready').css('animation', 'none');
    }
    //console.log(this.successTable.length + " / " + this.allInputs.length);
};

FormValidator.prototype.checkName = function(input) {
    var temp = input.value.split('');
    var success = temp.length ? true : false;
    var _this = this;

    // Uzywamy try-catch, bo wewnątrz Array.forEach() nie działa break;
    var BreakException = {};
    if (success) {
        try {
            temp.forEach(function(item, index) {
                if (index === 0) temp[index] = item.toUpperCase();
                else item = item.toLowerCase();
                if (item < 'A' || item > 'z') {
                    success = false;
                    _this.errorMessage = "W tym polu dozwolone są tylko litery";
                    throw BreakException;
                }
            });
        } catch (e) {
            if (e != BreakException) throw e;
        }
    } else {
        _this.errorMessage = "Nie mozesz zostawic tego pola pustego!";
    }

    // Poprawiamy value inputa (pierwsza litera duza itp..)
    input.value = temp.join('');
    return success;
};

FormValidator.prototype.checkEmail = function(input) {
    var temp = input.value.split('');
    var success = false;
    var monkeys = 0;
    var dotsAfterMonkey = 0;
    var isRestOkay = true;
    if (!temp.length) {
        this.errorMessage = "Nie mozesz zostawic tego pola pustego!";
        return false;
    }
    // Usuwamy spacje
    var index = 0;
    while ((index = temp.indexOf(' ')) > -1) {
        temp.splice(index, 1);
    }

    // W pętli sprawdzamy kazdy znak
    temp.forEach(function(item, index) {
        var ignoreRest = false;
        if (item === '@') {
            monkeys++;
            // Bezpośrednio przed małpą musi być cokolwiek
            if (!(temp[index - 1])) {
                isRestOkay = false;
            }
            // Bezpośrednio po małpie musi być litera
            if (!(temp[index + 1] && (temp[index + 1] >= 'A' && temp[index + 1] <= 'z'))) {
                isRestOkay = false;
            }
            ignoreRest = true;
        }
        if (!ignoreRest && monkeys === 0) {
            if (item === '.') {
                // Bezpośrednio po kropce musi być litera lub cyfra
                if (!(temp[index + 1] && ((temp[index + 1] >= 'A' && temp[index + 1] <= 'z') || (temp[index + 1] >= '0' && temp[index + 1] <= '9')))) {
                    isRestOkay = false;
                }
                ignoreRest = true;
            }
        }
        if (!ignoreRest && monkeys > 0) {
            if (item === '.') {
                dotsAfterMonkey++;
                // Bezpośrednio po drugiej kropce musi być cokolwiek (jesli to cyfra to i tak nam wywali w następnej iteracji)
                if (!(temp[index + 1])) {
                    isRestOkay = false;
                }
                ignoreRest = true;
            }
        }
        // Przed małpą mogą być litery, cyfry i kropki
        if (!ignoreRest && monkeys === 0) {
            if ((item < 'A' || item > 'z') && (item < '0' || item > '9')) {
                isRestOkay = false;
            }
        }
        // Po małpie tylko litery i cyfry
        if (!ignoreRest && monkeys > 0) {
            if (!((item >= 'A' && item <= 'z') || (item >= '0' && item <= '9'))) {
                isRestOkay = false;
            }
        }
    });

    // Podsumowanie
    if (monkeys === 1 && dotsAfterMonkey === 1 && isRestOkay) {
        success = true;
    } else {
        success = false;
    }

    // Łączymy i ewentualnie pooprawiamy value inputa (pierwsza litera duza itp..)
    input.value = temp.join('');
    if (!success) {
        this.errorMessage = "Podano zły adres e-mail.";
    }

    return success;
};

FormValidator.prototype.checkPassword = function(input) {
    var temp = input.value.split('');
    var success = (temp.length > 4) ? true : false;
    var _this = this;

    if (success) {
        // W pętli sprawdzamy kazdy znak
        var BreakException = {};
        try {
            temp.forEach(function(item, index) {
                // Hasło moze zawierać tylko litery i cyfry
                if ((item < 'A' || item > 'z') && (item < '0' || item > '9')) {
                    success = false;
                    _this.errorMessage = "Hasło moze zawierac tylko: \n-litery (a-z)\n -cyfry (0-9)";
                    throw BreakException;
                }
            });
        } catch (e) {
            if (e != BreakException) throw e;
        }
    } else {
        _this.errorMessage = "Hasło za krotkie.\nHaslo musi miec co najmniej 5 znakow!";
    }

    return success;
};

FormValidator.prototype.checkDate = function(input) {
    var temp = input.value.split('');
    var success = temp.length ? true : false;
    var _this = this;

    if (!success) {
        _this.errorMessage = "Nie mozesz zostawic tego pola pustego!";
        return success;
    }
    // Usuwamy myślniki, kropki, 'r'
    var index = 0;
    while ((index = temp.indexOf('-')) > -1 || (index = temp.indexOf('.')) > -1 || (index = temp.indexOf('r')) > -1) {
        temp.splice(index, 1);
    }
    if (temp.length < 6 || temp.length > 8) {
        _this.errorMessage = "Data powinna zawierac od 6 do 8 cyfr.\n";
        return false;
    }
    if (success) {
        var BreakException = {};
        try {
            temp.forEach(function(item, index) {
                if (item < '0' || item > '9') {
                    _this.errorMessage = "Data moze zawierac tylko cyfry, literę r, kropki oraz myślniki, np. 1.1.2001r.";
                    success = false;
                    throw BreakException;
                }
            });
        } catch (e) {
            if (e != BreakException) throw e;
        }
    }
    return success;
};

FormValidator.prototype.checkPhoneNumber = function(input) {
    var temp = input.value.split('');
    var success = temp.length ? true : false;
    var _this = this;

    if (!success) {
        _this.errorMessage = "Nie mozesz zostawic tego pola pustego!";
        return success;
    }

    // Usuwamy myślniki, spacje, nawiasy, plusy
    var index = 0;
    while ((index = temp.indexOf('-')) > -1 || (index = temp.indexOf(' ')) > -1 || (index = temp.indexOf('(')) > -1 || (index = temp.indexOf(')')) > -1 || (index = temp.indexOf('+')) > -1) {
        temp.splice(index, 1);
    }
    // Numer musi być w postaci '000000000', czyli kazdy element powienien być cyfrą i długość powinna zawierać się miedzy 9 a 12
    if (temp.length < 9 || temp.length > 12) {
        success = false;
        _this.errorMessage = "Numer telefonu powinien się składac z od 9 do 12 cyfr.";
        return success;
    }
    if (success) {
        try {
            temp.forEach(function(item, index) {
                if (item < '0' || item > '9') {
                    success = false;
                    _this.errorMessage = "Numer telefonu moze zawierac tylko cyfry, myślniki, spacje, nawiasy oraz znak + ";
                    throw BreakException;
                }
            });
        } catch (e) {
            if (e != BreakException) throw e;
        }
    }

    return success;
};

FormValidator.prototype.checkPostalCode = function(input) {
    var temp = input.value.split('');
    var success = temp.length ? true : false;
    var dashes = 0;
    var _this = this;

    // Usuwamy myślniki, spacje
    var index = 0;
    while ((index = temp.indexOf('-')) > -1 || (index = temp.indexOf(' ')) > -1) {
        temp.splice(index, 1);
    }
    if (success) {
        if (temp.length < 5) {
            _this.errorMessage = "Kod za krotki!";
            return false;
        }
        if (temp.length > 5) {
            _this.errorMessage = "Kod za długi!";
            return false;
        }
        var BreakException = {};
        try {
            temp.forEach(function(item, index) {
                if (item === '.') {
                    dashes++;
                }
                if (item < '0' || item > '9' || dashes > 1) {
                    success = false;
                    _this.errorMessage = "To pole moze zawierac tylko cyfry i jeden myślnik.";
                    throw BreakException;
                }
            });
        } catch (e) {
            if (e != BreakException) throw e;
        }
    } else {
      _this.errorMessage = "Nie mozesz zostawić tego pola pustego!";
    }

    return success;
};

FormValidator.prototype.checkInteger = function(input) {
    var temp = input.value.split('');
    var success = temp.length ? true : false;
    var _this = this;

    // Usuwamy spacje, przecinki
    var index = 0;
    while ((index = temp.indexOf(' ')) > -1 || (index = temp.indexOf(',')) > -1) {
        temp.splice(index, 1);
    }

    if (success) {
        // W pętli sprawdzamy kazdy znak
        var BreakException = {};
        try {
            temp.forEach(function(item, index) {
                // Integer moze zawierać tylko cyfry
                if (item < '0' || item > '9') {
                    success = false;
                    _this.errorMessage = "To pole moze zawierac tylko liczby całkowite.";
                    throw BreakException;
                }
            });
        } catch (e) {
            if (e != BreakException) throw e;
        }
    } else {
        _this.errorMessage = "Nie mozesz zostawic tego pola pustego!";
    }

    return success;
};

FormValidator.prototype.checkFloat = function(input) {
    var temp = input.value.split('');
    var success = temp.length ? true : false;
    var dots = 0;
    var _this = this;

    if (!success) {
        _this.errorMessage = "Nie mozesz zostawic tego pola pustego!";
        return success;
    }
    // Usuwamy spacje, przecinki
    var index = 0;
    while ((index = temp.indexOf(' ')) > -1 || (index = temp.indexOf(',')) > -1) {
        temp.splice(index, 1);
    }

    // W pętli sprawdzamy kazdy znak
    var BreakException = {};
    try {
        temp.forEach(function(item, index) {
            var ignoreRest = false;
            if (item === '.') {
                dots++;
                ignoreRest = true;
                // Bezpośrednio przed kropką musi coś być
                if (!temp[index - 1]) {
                    success = false;
                    _this.errorMessage = "Po kropce musi znajdowac się jakaś cyfra.";
                    throw BreakException;
                }
            }
            // Float moze zawierać tylko cyfry
            if ((!ignoreRest) && (item < '0' || item > '9')) {
                success = false;
                _this.errorMessage = "To pole moze zawierac tylko cyfry, spacje oraz jedną kropkę.";
                throw BreakException;
            }
        });
    } catch (e) {
        if (e != BreakException) throw e;
    }
    if (dots !== 0 && dots !== 1) {
        success = false;
        _this.errorMessage = "To pole moze zawierac tylko jedną kropkę!";
        throw BreakException;
    }

    // Łączymy i ewentualnie pooprawiamy value inputa (pierwsza litera duza itp..)
    input.value = temp.join('');
    return success;
};

FormValidator.prototype.printSuccesss = function(input) {
    // Za kazdym sukcesem usuwamy komunikaty o błędach
    $('.form-validator.message').remove();

    // Jeśli nie ma jeszcze keyframesa to podczepiamy
    if (!(this.keyframesSuccesReady)) {
        console.log("Tworzę keyframes SUCCESS i podczepiam w HEAD");
        this.keyframesSuccesReady = true;
        var keyframesSucces = document.createElement("STYLE");
        keyframesSucces.innerHTML = "@keyframes printSuccess {" +
            "                         0% {background: #FFF;}" +
            "                         100% {background: #CCFFCC;}" +
            "                        }";
        document.getElementsByTagName("HEAD")[0].appendChild(keyframesSucces);
    }
    $(input).css('animation', 'printSuccess 2s forwards');

    // Dodajemy do successTable (jeśli jeszcze tam nie występuje)
    if (this.successTable.indexOf(input) > -1) {
        return;
    } else {
        this.successTable.push(input);
    }
};

FormValidator.prototype.printFailure = function(input) {

    input.printMessage(this.errorMessage);

    // Jeśli nie ma jeszcze keyframesa to podczepiamy
    if (!(this.keyframesFailureReady)) {
        console.log("Tworzę keyframes FAILURE i podczepiam w HEAD");
        this.keyframesFailureReady = true;
        var keyframesFailure = document.createElement("STYLE");
        keyframesFailure.innerHTML = "@keyframes printFailure {" +
            "                         0% {background: #FFF;}" +
            "                         100% {background: #FFCCCC;}" +
            "                        }";
        document.getElementsByTagName("HEAD")[0].appendChild(keyframesFailure);
    }
    $(input).css('animation', 'printFailure 2s forwards');
    var index = -1;

    // Usuwamy z successTable (jeśli tam występuje)
    if ((index = this.successTable.indexOf(input)) > -1) {
        this.successTable.splice(index, 1);
    } else {
        return;
    }

};

Element.prototype.printMessage = function(message) {
    $('.form-validator.message').remove();
    var messageDivStyles = document.createElement('STYLE');
    var inputPosition = $(this).position();
    messageDivStyles.innerHTML = ".form-validator.message {" +
        "                               width: 250px;" +
        "                               height: 75px;" +
        "                               border: 2px solid #f00;" +
        "                               border-radius: 10px;" +
        "                               font-size: 12px;" +
        "                               background: #fcc;" +
        "                               opacity: 0.8;" +
        "                               position: fixed; top: " + (inputPosition.top - 93) + "px;" +
        "                               left: " + (inputPosition.left + 250) + "px;" +
        "                               padding: 5px 5px 0 5px;" +
        "                               text-align: left;" +
        "                             }" +
        "                             .form-validator.message:after {" +
        "                               content: ''; width: 0; height: 0;" +
        "                               border: 5px solid #f00;" +
        "                               border-top: 10px solid #f00;" +
        "                               border-bottom: 10px solid transparent;" +
        "                               border-left: 10px solid transparent;" +
        "                               border-right: 10px solid transparent;" +
        "                               position: fixed; top: " + (inputPosition.top - 10) + "px;" +
        "                               left: " + (inputPosition.left + 275) + "px;" +
        "                             }";
    document.getElementsByTagName('HEAD')[0].appendChild(messageDivStyles);
    var messageDiv = document.createElement('DIV');
    messageDiv.className = "form-validator message";
    messageDiv.appendChild(document.createTextNode(message));
    $(this).before(messageDiv);
};
